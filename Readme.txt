This is a contruction of a Lexical analyzer and a scanner in part of competleting a compiler. Lexical analysis is the first phase of a compiler.
It takes the modified source code from language preprocessors that are written in the form of sentences.
The scanner examines the input of text, which then separates the source program into parts called tokens. 
These tokens represent variable names, operator, labels and so one that comprise the source program.
If the lexical analyzer finds a token invalid, it generates an error. The lexical analyzer works closely with the syntax analyzer. 
It reads character streams from the source code, checks for legal tokens, and passes the data to the syntax analyzer when it demands.
The lexical analyzer breaks these syntaxes into a series of tokens, by removing any whitespace or comments in the source code.

The code will be done through Java SE8 and will be using JFlex-1.6.1 to implement source code. 

Steps
1)  JFlex (source code) 
2)  The lexical analysis 
3)  Scanner will scan code
4)  Output of code


