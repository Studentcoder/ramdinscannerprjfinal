/*This is a sample
* Vinod No Errors
*/

/*Declarations */

%%

%standalone 			/* the produced java file has a main */
%class MyScanner 
%function nextToken
%type String 
%eofval{
  return null;
%eofval}

/* Pattern */

letter	= [A-Za-z]
word	= {letter}+
digit = [0-9]
number = {digit}+
whitespace	= [ \n\t]
other = . /*any single char*/


%%

/* Lexical Rules */

{word}  {
		  System.out.println("Found a word: " + yytext());
		  }
		  
{whitespace} { /*Ignore Whitespace */ 
				return "";
			}
						  
{other}		{
			   System.out.println("Illegal char: '" + yytext() + "' found. ");
			   //return "";
			   }	
{number}  {
			   System.out.println("Found a digit: '" + yytext());
			   //return "";
			   }